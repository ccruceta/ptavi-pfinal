#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import socket
import sys
import threading
import time

IP = '0.0.0.0'
PORT = 0


def logs(event):
    """Funcion para mostrar los mensajes historicos en formato correcto"""
    t = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))

    if event == 'Starting...':
        return sys.stdout.write(t + ' ' + event + '\r\n')
    else:
        if list(event)[-4] == '\r':
            event = event.split('\r\n')
            event[-3] = event[-3] + '.'
            event = '\r\n'.join(event)
        else:
            event = event.split('\r\n')
            event[-2] = event[-2] + '.'
            event = '\r\n'.join(event)

        return sys.stdout.write(t + ' ' + event + '\r\n')


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes que nos lleguen.

    Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente."""

    output = None

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)

    @classmethod
    def open_output(cls, filename):
        """Abrimos el fichero donde se va a escribir lo recibido."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerramos el fichero donde se va a escribir lo recibido."""
        cls.output.close()


def main():
    """M´etodo principal"""
    if len(sys.argv) != 6:
        print("Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")
        exit()
    else:
        try:
            server_sip = sys.argv[1]
            addr_client = sys.argv[2]
            addr_serv_rtp = sys.argv[3]
            expire = int(sys.argv[4])
            file = sys.argv[5]

        except (IndexError, ValueError):
            sys.exit("Usage: python3 client.py <IPServerSIP>:<portServerSIP> "
                     "<addrClient> <addrServerRTP> <time> <file>")

        ip_serv_sip = server_sip.split(':')[0]
        port_serv_sip = server_sip.split(':')[1]
        sip_name = addr_serv_rtp.split(':')[1]
        sip_name = sip_name.split('@')[0]

        # PARTE SIP
        try:

            logs('Starting...')
            with socketserver.UDPServer((IP, PORT), RTPHandler) as my_socket:
                rtp_ip = my_socket.server_address[0]
                rtp_port = my_socket.server_address[1]

            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sip_socket:

                # ENVIO INVITE

                message = ('INVITE ' + addr_serv_rtp + ' SIP/2.0' + "\r\n")
                sdp = "v=0\r\no=" + addr_client + " " + rtp_ip + "\r\n"
                sdp += "s=" + sip_name + "\r\nt=0\r\nm=audio " + str(rtp_port) + " RTP\r\n"
                cab = "Content-Type: application/sdp\r\n"
                cab += "Content-Length: " + str(len(bytes(sdp, 'utf-8'))) + "\r\n\r\n"
                message = message + cab + sdp
                sip_socket.sendto(message.encode('utf-8'), (ip_serv_sip, int(port_serv_sip)))
                logs('SIP to ' + ip_serv_sip + ':' + str(port_serv_sip) + ' '
                     + message)

                # RECIBO REDIRECT
                data = sip_socket.recv(1024)
                line = data.decode('utf-8')
                reply = line.split('\r\n')
                new_rtp_serv = reply[1].split(' ')[1]
                new_rtp_serv_ip = new_rtp_serv.split('@')[1].split(':')[0]
                new_rtp_serv_port = new_rtp_serv.split('@')[1].split(':')[1]

                if reply[0] == 'SIP/2.0 302 Moved Temporarily':
                    logs('SIP from ' + ip_serv_sip + ':' + str(port_serv_sip) + ' '
                         + line)

                    # ENVIO ACK
                    message = ('ACK ' + addr_serv_rtp + ' SIP/2.0' + "\r\n\r\n")
                    sip_socket.sendto(message.encode('utf-8'), (ip_serv_sip, int(port_serv_sip)))
                    logs('SIP to ' + ip_serv_sip + ':' + str(port_serv_sip) + ' '
                         + message)

                    # ENVIO el NUEVO INVITE
                    message = ('INVITE ' + new_rtp_serv + ' SIP/2.0' + "\r\n")
                    sdp = "v=0\r\no=" + addr_client + " " + rtp_ip + "\r\n"
                    sdp += "s=" + sip_name + "\r\nt=0\r\nm=audio " + str(rtp_port) + " RTP\r\n"
                    cab = "Content-Type: application/sdp\r\n"
                    cab += "Content-Length: " + str(len(bytes(sdp, 'utf-8'))) + "\r\n\r\n"
                    message = message + cab + sdp
                    sip_socket.sendto(message.encode('utf-8'), (new_rtp_serv_ip, int(new_rtp_serv_port)))
                    logs('SIP to ' + new_rtp_serv_ip + ':' + str(new_rtp_serv_port) + ' '
                         + message)

                    # RECIBO 200 OK
                    data = sip_socket.recv(1024)
                    line = data.decode('utf-8')
                    reply = line.split('\r\n')

                    if reply[0] == 'SIP/2.0 200 OK':
                        logs('SIP from ' + new_rtp_serv_ip + ':' + str(new_rtp_serv_port) + ' '
                             + line)

                        # ENVIO ACK
                        message = ('ACK ' + new_rtp_serv + ' SIP/2.0' + "\r\n\r\n")
                        sip_socket.sendto(message.encode('utf-8'), (new_rtp_serv_ip, int(new_rtp_serv_port)))
                        logs('SIP to ' + new_rtp_serv_ip + ':' + str(new_rtp_serv_port) + ' '
                             + message)

                        # PARTE RTP
                        # Abrimos un fichero para escribir los datos que se reciban
                        RTPHandler.open_output(file)

                        # Bucle de recepción que va en un hilo aparte
                        # Comenzamos a esperar paquetes RTP
                        with socketserver.UDPServer((rtp_ip, rtp_port), RTPHandler) as rtp_socket:
                            # RECIBO CARGA RTP
                            threading.Thread(target=rtp_socket.serve_forever).start()
                            logs('RTP ready ' + str(rtp_port) + '\r\n')

                            # Declaramos el tiempo que pasa hasta que enviamos el BYE
                            time.sleep(expire)

                            # ENVIO BYE
                            message = ('BYE ' + new_rtp_serv + ' SIP/2.0' + "\r\n\r\n")
                            sip_socket.sendto(message.encode('utf-8'), (new_rtp_serv_ip, int(new_rtp_serv_port)))
                            logs('SIP to ' + new_rtp_serv_ip + ':' + str(new_rtp_serv_port) + ' '
                                 + message)
                            # RECIBO 200 OK
                            data = sip_socket.recv(1024)
                            line = data.decode('utf-8')
                            reply = line.split('\r\n')
                            if reply[0] == 'SIP/2.0 200 OK':
                                logs('SIP from ' + new_rtp_serv_ip + ':' + str(new_rtp_serv_port) + ' '
                                     + line)

                                # Paramos el bucle de recepción, con lo que terminará el thread,
                                # y dejamos de recibir paquetes
                                rtp_socket.shutdown()

                        # Cerramos el fichero donde estamos escribiendo los datos recibidos
                        sip_socket.close()
                        RTPHandler.close_output()
                        quit()
        except ConnectionRefusedError:
            print("Error conectando a servidor")


if __name__ == "__main__":
    main()
