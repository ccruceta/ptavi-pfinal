#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socketserver
import socket
import sys
import simplertp
import time

IP = "0.0.0.0"
PORT = 0


def logs(event):
    """Funcion para mostrar los mensajes historicos en formato correcto"""
    t = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))

    if event == 'Starting...':
        return sys.stdout.write(t + ' ' + event + '\r\n')
    else:
        if list(event)[-4] == '\r':
            event = event.split('\r\n')
            event[-3] = event[-3] + '.'
            event = '\r\n'.join(event)
        else:
            event = event.split('\r\n')
            event[-2] = event[-2] + '.'
            event = '\r\n'.join(event)

        return sys.stdout.write(t + ' ' + event + '\r\n')


class SIPHandler(socketserver.BaseRequestHandler):
    sender = None
    client_rtp_ip = None
    client_rtp_port = None

    def handle(self):

        data = self.request[0]
        sock = self.request[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        reply = data.decode('utf-8')
        line = reply.split('\r\n')
        file = sys.argv[3]

        # RECIBO 200 OK
        if line[0] == 'SIP/2.0 200 OK':
            logs('SIP from ' + client_ip + ':' + str(client_port) + ' '
                 + reply)
        # RECIBO INVITE
        elif line[0].split(' ')[0] == 'INVITE':
            SIPHandler.client_rtp_ip = line[5].split(' ')[1]
            SIPHandler.client_rtp_port = line[8].split(' ')[1]
            logs('SIP from ' + client_ip + ':' + str(client_port) + ' '
                 + reply)
            # ENVIO OK
            message = 'SIP/2.0 200 OK\r\n\r\n'
            sock.sendto(message.encode('utf-8'), (client_ip, int(client_port)))
            logs('SIP to ' + client_ip + ':' + str(client_port) + ' '
                 + message)

        # RECIBO ACK
        elif line[0].split(' ')[0] == 'ACK':
            logs('SIP from ' + client_ip + ':' + str(client_port) + ' '
                 + reply)
            # ENVIO RTP
            SIPHandler.sender = simplertp.RTPSender(SIPHandler.client_rtp_ip,
                                                    int(SIPHandler.client_rtp_port), file, printout=False)
            SIPHandler.sender.send_threaded()
            logs('RTP to ' + SIPHandler.client_rtp_ip + ':' + str(SIPHandler.client_rtp_port) + '\r\n')

        # RECIBO BYE, ENVIO OK Y FINALIZO ENVIO
        elif line[0].split(' ')[0] == 'BYE':
            logs('SIP from ' + client_ip + ':' + str(client_port) + ' '
                 + reply)
            SIPHandler.sender.finish()
            message = 'SIP/2.0 200 OK\r\n\r\n'
            sock.sendto(message.encode('utf-8'), (client_ip, int(client_port)))
            logs('SIP to ' + client_ip + ':' + str(client_port) + ' '
                 + message)

        else:
            message = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
            sock.sendto(message.encode('utf-8'), (client_ip, int(client_port)))
            logs('SIP to ' + client_ip + ':' + str(client_port) + ' '
                 + message)


def main():
    if len(sys.argv) != 4:
        print('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>')
        exit()
    else:
        try:
            serv_sip_ip = sys.argv[1].split(':')[0]
            serv_sip_port = sys.argv[1].split(':')[1]
            service = sys.argv[2]

        except (IndexError, ValueError):
            sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>')

        try:
            logs('Starting...')
            # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                # ENVIO REGISTER
                message = ('REGISTER sip:' + service + '@songs.net SIP/2.0\r\n\r\n')
                my_socket.sendto(message.encode('utf-8'), (serv_sip_ip, int(serv_sip_port)))
                logs('SIP to ' + serv_sip_ip + ':' + str(serv_sip_port) + ' '
                     + message)
                name = my_socket.getsockname()
                ip = name[0]
                port = name[1]
        except ConnectionRefusedError:
            print("Error conectando a servidor")
        server = socketserver.UDPServer((ip, int(port)), SIPHandler)

        try:
            server.serve_forever()
        except KeyboardInterrupt:
            pass


if __name__ == "__main__":
    main()
