## Parte básica
La parte básica de la práctica está terminada en su totalidad. El enlace para acceder al commit previo a las modificaciones de la parte adicional es el siguiente: 2fb4a57812607a95b0e01fac67b7aefb2349dc8d 

## Parte adicional
* Cabecera de tamaño
Para llevar a cabo esta opción solo se ha tenido que modificar el código relacionado con los dos mensajes INVITE que envía el cliente, ya que solo estos mensajes SIP tienen cuerpo. Añadimos entonces, la cabecera "Content-Length", que contiene el tamaño en bytes del cuerpo del mensaje. 

* Gestión de errores
Para implementar esta funcionalidad añadimos las respuestas "400 Bad Request", "405 Method Not Allowed" y "404 User Not Found" en los programas serversip.py y serverrtp.py dependiendo del error que se cometa al recibir los datos.
