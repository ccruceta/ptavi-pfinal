#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socketserver
import sys
import json
import time

IP = "127.0.0.1"


def logs(event):
    """Funcion para mostrar los mensajes historicos en formato correcto"""
    t = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))

    if event == 'Starting...':
        return sys.stdout.write(t + ' ' + event + '\r\n')
    else:
        if list(event)[-4] == '\r':
            event = event.split('\r\n')
            event[-3] = event[-3] + '.'
            event = '\r\n'.join(event)
        else:
            event = event.split('\r\n')
            event[-2] = event[-2] + '.'
            event = '\r\n'.join(event)

        return sys.stdout.write(t + ' ' + event + '\r\n')


class SIPHandler(socketserver.BaseRequestHandler):
    dicc = {}

    @classmethod
    def dicc2json(cls):
        """Funcion para volcar el diccionario en un archivo json"""
        with open("registrar.json", "w") as jsonfile:
            json.dump(cls.dicc, jsonfile, indent=4)

    @classmethod
    def json2dicc(cls):
        """Funcion para actualizar diccionario con archivo json"""
        try:

            with open("registrar.json", "r") as jsonfile:
                cls.dicc = json.load(jsonfile)

        except FileNotFoundError:
            pass

    def handle(self):

        data = self.request[0]
        sock = self.request[1]
        client_message = data.decode('utf-8')
        self.json2dicc()

        # RECIBO MENSAJE
        if client_message != '\r\n':
            try:
                client_elements = client_message.split(' ')
                method = client_elements[0]
                address = client_elements[1]
                new_address = address.split('@')[0] + '@' + self.client_address[0] + ':' + str(self.client_address[1])
                logs('SIP from ' + self.client_address[0] + ':' + str(self.client_address[1]) + ' '
                     + client_message)
                # RECIBO REGISTER
                if method == 'REGISTER':
                    SIPHandler.dicc[address] = new_address
                    self.dicc2json()
                    message = 'SIP/2.0 200 OK\r\n\r\n'
                    # ENVIO 200 OK
                    logs('SIP to ' + self.client_address[0] + ':' + str(self.client_address[1]) + ' '
                         + message)
                    reply = bytes(message, "utf-8")
                    sock.sendto(reply, self.client_address)

                # RECIBO INVITE
                elif method == 'INVITE':
                    if address in SIPHandler.dicc:
                        message = ('SIP/2.0 302 Moved Temporarily\r\nContact: ' + SIPHandler.dicc[address] + "\r\n\r\n")
                        # ENVIO REDIRECT
                        logs('SIP to ' + self.client_address[0] + ':' + str(self.client_address[1]) + ' '
                             + message)
                        reply = bytes(message, "utf-8")
                        sock.sendto(reply, self.client_address)
                    else:
                        message = "SIP/2.0 404 User Not Found\r\n\r\n"
                        logs('SIP to ' + self.client_address[0] + ':' + str(self.client_address[1]) + ' '
                             + message)
                        reply = bytes(message, "utf-8")
                        sock.sendto(reply, self.client_address)

                elif method == 'ACK':
                    pass
                else:
                    message = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
                    logs('SIP to ' + self.client_address[0] + ':' + str(self.client_address[1]) + ' '
                         + message)
                    reply = bytes(message, "utf-8")
                    sock.sendto(reply, self.client_address)

            except IndexError:
                message = "SIP/2.0 400 Bad Request\r\n\r\n"
                logs('SIP to ' + self.client_address[0] + ':' + str(self.client_address[1]) + ' '
                     + message)
                reply = bytes(message, "utf-8")
                sock.sendto(reply, self.client_address)


def main():
    if len(sys.argv) != 2:
        print('Usage: python3 serversip.py <port>')
        exit()
    else:

        try:
            port = sys.argv[1]
            logs('Starting...')
            server = socketserver.UDPServer((IP, int(port)), SIPHandler)
        except (IndexError, ValueError):
            sys.exit('Usage: python3 serversip.py <port>')

        try:
            server.serve_forever()
        except KeyboardInterrupt:
            pass


if __name__ == "__main__":
    main()
